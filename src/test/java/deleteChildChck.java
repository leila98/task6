import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import src.com.company.*;


public class deleteChildChck {


    @Test
    public void checkDeleted() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        String[] habits1 = {"eat", "sleep"};
        Pet pet1 = new Pet(Species.Dog, "toplan", 3, 78, habits1);
        int[][] schedule1 = new int[7][1];
        String arr1[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to school"},
                {String.valueOf(DayOftheWeek.Tuesday), "Read a book"},
                {String.valueOf(DayOftheWeek.Wednesday), "Buy the book"},
                {String.valueOf(DayOftheWeek.Thursday), "Drink a coffee"},
                {String.valueOf(DayOftheWeek.Friday), "Meet with friends"},
                {String.valueOf(DayOftheWeek.Saturday), "Go to store"},
                {String.valueOf(DayOftheWeek.Sunday), "Buy furniture"}};


        Human child1 = new Human("leila", "mammadova", 1998, 88, pet1, mother1, father1, arr1);


        String arr2[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to the gym"},
                {String.valueOf(DayOftheWeek.Tuesday), "Write a code"},
                {String.valueOf(DayOftheWeek.Wednesday), "Do delivering"},
                {String.valueOf(DayOftheWeek.Thursday), "Do homework"},
                {String.valueOf(DayOftheWeek.Friday), "Playing football"},
                {String.valueOf(DayOftheWeek.Saturday), "Meet with friends"},
                {String.valueOf(DayOftheWeek.Sunday), "Go to breakfast"}};


        Human child2 = new Human("anar", "rzayev", 2007, 43, pet1, mother1, father1, arr2);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, pet1, mother1, father1, arr2);

        Family family = new Family(mother1, father1, new Human[]{child1, child2, child3});


        boolean deleted = family.deleteChild(child1);
        Assertions.assertTrue(deleted == true);
    }


    @Test
    public void checkDeleted2() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        String[] habits1 = {"eat", "sleep"};
        Pet pet1 = new Pet(Species.Dog, "toplan", 3, 78, habits1);
        int[][] schedule1 = new int[7][1];
        String arr1[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to school"},
                {String.valueOf(DayOftheWeek.Tuesday), "Read a book"},
                {String.valueOf(DayOftheWeek.Wednesday), "Buy the book"},
                {String.valueOf(DayOftheWeek.Thursday), "Drink a coffee"},
                {String.valueOf(DayOftheWeek.Friday), "Meet with friends"},
                {String.valueOf(DayOftheWeek.Saturday), "Go to store"},
                {String.valueOf(DayOftheWeek.Sunday), "Buy furniture"}};


        Human child1 = new Human("leila", "mammadova", 1998, 88, pet1, mother1, father1, arr1);


        String arr2[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to the gym"},
                {String.valueOf(DayOftheWeek.Tuesday), "Write a code"},
                {String.valueOf(DayOftheWeek.Wednesday), "Do delivering"},
                {String.valueOf(DayOftheWeek.Thursday), "Do homework"},
                {String.valueOf(DayOftheWeek.Friday), "Playing football"},
                {String.valueOf(DayOftheWeek.Saturday), "Meet with friends"},
                {String.valueOf(DayOftheWeek.Sunday), "Go to breakfast"}};


        Human child2 = new Human("anar", "rzayev", 2007, 43, pet1, mother1, father1, arr2);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, pet1, mother1, father1, arr2);

        Family family = new Family(mother1, father1, new Human[]{child1, child2, child3});


        boolean deleted = family.deleteChild(child1);
        Assertions.assertTrue(deleted == true);
    }


    @Test
    public void checkDeletedWithIndex() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        String[] habits1 = {"eat", "sleep"};
        Pet pet1 = new Pet(Species.Dog, "toplan", 3, 78, habits1);
        int[][] schedule1 = new int[7][1];
        String arr1[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to school"},
                {String.valueOf(DayOftheWeek.Tuesday), "Read a book"},
                {String.valueOf(DayOftheWeek.Wednesday), "Buy the book"},
                {String.valueOf(DayOftheWeek.Thursday), "Drink a coffee"},
                {String.valueOf(DayOftheWeek.Friday), "Meet with friends"},
                {String.valueOf(DayOftheWeek.Saturday), "Go to store"},
                {String.valueOf(DayOftheWeek.Sunday), "Buy furniture"}};


        Human child1 = new Human("leila", "mammadova", 1998, 88, pet1, mother1, father1, arr1);


        String arr2[][] = {{String.valueOf(DayOftheWeek.Monday), "Go to the gym"},
                {String.valueOf(DayOftheWeek.Tuesday), "Write a code"},
                {String.valueOf(DayOftheWeek.Wednesday), "Do delivering"},
                {String.valueOf(DayOftheWeek.Thursday), "Do homework"},
                {String.valueOf(DayOftheWeek.Friday), "Playing football"},
                {String.valueOf(DayOftheWeek.Saturday), "Meet with friends"},
                {String.valueOf(DayOftheWeek.Sunday), "Go to breakfast"}};


        Human child2 = new Human("anar", "rzayev", 2007, 43, pet1, mother1, father1, arr2);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, pet1, mother1, father1, arr2);

        Family family = new Family(mother1, father1, new Human[]{child1, child2, child3});


        boolean deleted = family.deleteChild2(1);
        Assertions.assertTrue(deleted == true);
    }


}
